﻿function AvoidSpace(event) {
    var k = event ? event.which : window.event.keyCode;
    if (k == 32) {
        return false;
    }
}

$("#btnRefresh").click(function () {
    window.location.reload();
});


// Code for Saving Customer Information
$("#btnCustomerAdd").click(function () {
    var _fullname = $("#Full_Name").val();
    var _Address = $("#Address").val();
    var _mobile = $("#Mobile").val();
    var _email = $("#Email").val();

    if (_fullname == '') {
        $("#divFailour").fadeIn(400).delay(1000).fadeOut(400);
        $("#lblFailour").text("Please enter name");
        $("#Full_Name").focus();
        return false;
    }
    if (!isNaN(_fullname)) {
        $("#Full_Name").val("");
        $("#Full_Name").focus();
        return false;
    }

    if (_Address == '') {
        $("#divFailour").fadeIn(400).delay(1000).fadeOut(400);
        $("#lblFailour").text("Please enter address");
        $("#Address").focus();
        return false;
    }
    if (_mobile == '') {
        $("#divFailour").fadeIn(400).delay(1000).fadeOut(400);
        $("#lblFailour").text("Please enter Mobile");
        $("#Mobile").focus();
        return false;
    }
    if (_email == '') {
        $("#divFailour").fadeIn(400).delay(1000).fadeOut(400);
        $("#lblFailour").text("Please enter Email");
        $("#Email").focus();
        return false;
    }
    var _data = $("#frmAddCustomer").serialize();
    $.ajax({
        type: "POST",
        url: "/Master/AddCustomer",
        dataType: "JSON",
        data: _data,
        success: function (da) {
            if (da == "Success") {
                $("#divSuccess").fadeIn(400).delay(1000).fadeOut(400);
                $("#divSuccess").text("Data Saved successfully.");
                setTimeout(function () {
                    window.location.reload();
                }, 1500);
            }
            else {
                $("#divFailour").fadeIn(400).delay(1000).fadeOut(400);
                $("#divFailour").text("Incorrect Data ! Try again..");
                //setTimeout(function () {
                //    window.location.reload();
                //}, 1500);
            }
        }
    });

});
