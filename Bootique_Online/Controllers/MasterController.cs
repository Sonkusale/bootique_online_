﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Business_Logic_layer;
using Data_Access_Layer;

namespace Bootique_Online.Controllers
{
    public class MasterController : Controller
    {
        Customer_DAL methods = new Customer_DAL();
        // GET: Master
        public ActionResult NewCustomer()
        {
            var dt = methods.GetAll_Customer();
            return View(dt);
        }

        [HttpPost]
        public JsonResult AddCustomer(Customer_BAL customer)
        {
            var result = methods.Add_Customer(customer);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult Sale()
        {
            return View();
        }

        public ActionResult Alteration()
        {
            return View();
        }

        public ActionResult Ledger()
        {
            return View();
        }

        public ActionResult SMS()
        {
            return View();
        }
    }
}