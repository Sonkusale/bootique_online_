USE [Online_Bootique]
GO
/****** Object:  Table [dbo].[Admin_Register]    Script Date: 21-06-2019 13:49:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin_Register](
	[Sr_Number] [int] NULL,
	[Office_ID] [nvarchar](20) NULL,
	[Admin_id] [nvarchar](12) NULL,
	[Admin_Name] [nvarchar](100) NULL,
	[Mobile] [nvarchar](12) NULL,
	[Email] [nvarchar](40) NULL,
	[UserName] [nvarchar](25) NULL,
	[Password] [varbinary](max) NULL,
	[DOB] [nvarchar](25) NULL,
	[AGE] [nvarchar](25) NULL,
	[Other] [nvarchar](100) NULL,
	[Create_Date] [nvarchar](25) NULL,
	[Create_Date_UserName] [nvarchar](35) NULL,
	[Update_Date] [nvarchar](25) NULL,
	[Update_Date_UserName] [nvarchar](35) NULL,
	[Delete_Date] [nvarchar](25) NULL,
	[Delete_Date_UserName] [nvarchar](35) NULL,
	[Delete_Why] [nvarchar](100) NULL,
	[status] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Alteration]    Script Date: 21-06-2019 13:49:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Alteration](
	[Sr_Number] [int] NULL,
	[Office_ID] [nvarchar](20) NULL,
	[Alteration_id] [int] NULL,
	[Deli_Date] [nvarchar](30) NULL,
	[Name] [nvarchar](60) NULL,
	[Mobile] [nvarchar](12) NULL,
	[Perticular] [nvarchar](30) NULL,
	[Alteration] [nvarchar](100) NULL,
	[Rate] [money] NULL,
	[Quantity] [int] NULL,
	[MRP_Quantity] [float] NULL,
	[Total_Amount] [money] NULL,
	[Balance] [money] NULL,
	[Advance] [money] NULL,
	[Other] [nvarchar](100) NULL,
	[Create_Date] [nvarchar](25) NULL,
	[Create_Date_UserName] [nvarchar](35) NULL,
	[Update_Date] [nvarchar](25) NULL,
	[Update_Date_UserName] [nvarchar](35) NULL,
	[Delete_Date] [nvarchar](25) NULL,
	[Delete_Date_UserName] [nvarchar](35) NULL,
	[Delete_Why] [nvarchar](100) NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 21-06-2019 13:49:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Office_ID] [nvarchar](20) NULL,
	[sr_number] [int] NOT NULL,
	[Customer_ID] [int] NOT NULL,
	[Full_Name] [nvarchar](100) NOT NULL,
	[Address] [nvarchar](100) NULL,
	[Mobile] [nvarchar](10) NULL,
	[Email] [nvarchar](50) NULL,
	[DOB] [nvarchar](25) NULL,
	[AGE] [int] NULL,
	[Create_Date] [nvarchar](25) NULL,
	[Create_Date_UserName] [nvarchar](35) NULL,
	[Update_Date] [nvarchar](25) NULL,
	[Update_Date_UserName] [nvarchar](35) NULL,
	[Delete_Date] [nvarchar](25) NULL,
	[Delete_Date_UserName] [nvarchar](35) NULL,
	[Delete_Why] [nvarchar](100) NULL,
	[Status] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Expense]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Expense](
	[Office_ID] [nvarchar](20) NULL,
	[Sr_Number] [int] NULL,
	[Expense_id] [int] NULL,
	[Category] [nvarchar](30) NULL,
	[Other] [nvarchar](100) NULL,
	[income_amount] [money] NULL,
	[expense_amount] [money] NULL,
	[Dates] [nvarchar](30) NULL,
	[Create_Date] [nvarchar](25) NULL,
	[Create_Date_UserName] [nvarchar](35) NULL,
	[Update_Date] [nvarchar](25) NULL,
	[Update_Date_UserName] [nvarchar](35) NULL,
	[Delete_Date] [nvarchar](25) NULL,
	[Delete_Date_UserName] [nvarchar](35) NULL,
	[Delete_Why] [nvarchar](100) NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ledger]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ledger](
	[sr_number] [int] NULL,
	[Office_ID] [nvarchar](20) NULL,
	[ledger_id] [nvarchar](10) NULL,
	[voucher_id] [nvarchar](30) NULL,
	[save_date] [nvarchar](30) NULL,
	[cust_supp_id] [nvarchar](14) NULL,
	[party_name] [nvarchar](70) NULL,
	[bill_amount] [nvarchar](14) NULL,
	[paid_amount] [nvarchar](14) NULL,
	[balance_amount] [nvarchar](15) NULL,
	[remark] [nvarchar](100) NULL,
	[save_time] [nvarchar](14) NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[orders]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[orders](
	[sr_number] [int] NULL,
	[Office_ID] [nvarchar](20) NULL,
	[orderId] [int] NULL,
	[full_name] [nvarchar](100) NULL,
	[address] [nvarchar](100) NULL,
	[mobile] [nvarchar](20) NULL,
	[perticular] [nvarchar](60) NULL,
	[length] [nvarchar](20) NULL,
	[waist] [nvarchar](20) NULL,
	[seat] [nvarchar](20) NULL,
	[bottom] [nvarchar](20) NULL,
	[knee] [nvarchar](20) NULL,
	[thies] [nvarchar](20) NULL,
	[rounds] [nvarchar](20) NULL,
	[i_length] [nvarchar](20) NULL,
	[waist1] [nvarchar](20) NULL,
	[chest] [nvarchar](20) NULL,
	[waist2] [nvarchar](20) NULL,
	[sholder] [nvarchar](20) NULL,
	[sleeves] [nvarchar](20) NULL,
	[neck] [nvarchar](20) NULL,
	[front] [nvarchar](20) NULL,
	[kaf] [nvarchar](20) NULL,
	[frnt_neck] [nvarchar](20) NULL,
	[back_neck] [nvarchar](20) NULL,
	[dot] [nvarchar](20) NULL,
	[slvs_round] [nvarchar](20) NULL,
	[other] [nvarchar](100) NULL,
	[quantity] [nvarchar](20) NULL,
	[rate] [nvarchar](20) NULL,
	[totalPay] [nvarchar](20) NULL,
	[advance] [nvarchar](20) NULL,
	[balance] [nvarchar](20) NULL,
	[save_date] [nvarchar](30) NULL,
	[deli_date] [nvarchar](30) NULL,
	[save_time] [nvarchar](35) NULL,
	[temp_id] [nvarchar](50) NULL,
	[mrpQuantity] [nvarchar](20) NULL,
	[gher] [nvarchar](15) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Perticular]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Perticular](
	[Sr_Number] [int] NULL,
	[Office_ID] [nvarchar](20) NULL,
	[Perticular_id] [int] NULL,
	[Perticular] [nvarchar](30) NULL,
	[GroupName] [nvarchar](30) NULL,
	[Other] [nvarchar](100) NULL,
	[Create_Date] [nvarchar](25) NULL,
	[Create_Date_UserName] [nvarchar](35) NULL,
	[Update_Date] [nvarchar](25) NULL,
	[Update_Date_UserName] [nvarchar](35) NULL,
	[Delete_Date] [nvarchar](25) NULL,
	[Delete_Date_UserName] [nvarchar](35) NULL,
	[Delete_Why] [nvarchar](100) NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SendMessages]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SendMessages](
	[Sr_Number] [int] NULL,
	[Office_ID] [nvarchar](20) NULL,
	[Messages_id] [int] NULL,
	[Messages] [nvarchar](200) NULL,
	[Other] [nvarchar](100) NULL,
	[Create_Date] [nvarchar](25) NULL,
	[Create_Date_UserName] [nvarchar](35) NULL,
	[Update_Date] [nvarchar](25) NULL,
	[Update_Date_UserName] [nvarchar](35) NULL,
	[Delete_Date] [nvarchar](25) NULL,
	[Delete_Date_UserName] [nvarchar](35) NULL,
	[Delete_Why] [nvarchar](100) NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplier](
	[Sr_Number] [int] NULL,
	[Office_ID] [nvarchar](20) NULL,
	[Supplier_id] [int] NULL,
	[Supplier_Name] [nvarchar](100) NULL,
	[Company_Name] [nvarchar](50) NULL,
	[Mobile] [nvarchar](12) NULL,
	[Email] [nvarchar](40) NULL,
	[Other] [nvarchar](100) NULL,
	[Create_Date] [nvarchar](25) NULL,
	[Create_Date_UserName] [nvarchar](35) NULL,
	[Update_Date] [nvarchar](25) NULL,
	[Update_Date_UserName] [nvarchar](35) NULL,
	[Delete_Date] [nvarchar](25) NULL,
	[Delete_Date_UserName] [nvarchar](35) NULL,
	[Delete_Why] [nvarchar](100) NULL,
	[status] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Admin_Register] ADD  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[Alteration] ADD  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[Customer] ADD  CONSTRAINT [DF__Customer__Status__6477ECF3]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[Expense] ADD  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[Ledger] ADD  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[Perticular] ADD  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[SendMessages] ADD  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[Supplier] ADD  DEFAULT ((1)) FOR [status]
GO
/****** Object:  StoredProcedure [dbo].[CRUD_Admin]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[CRUD_Admin]
(
    @Sr_Number int = NULL,
	@Office_ID nvarchar(20) = null,

	@Admin_id nvarchar(12) = null,
	@Admin_Name nvarchar(100) = null,
	@Mobile nvarchar(12) = null,
	@Email nvarchar(40) = null,
	@UserName nvarchar(25) = null,
	@Password varbinary(max) = null,
	@DOB nvarchar(25) = null,
	@AGE int = Null,
	@Password_New varbinary(max) = null,

	@Other nvarchar(100) = NULL,
	@income_amount money = NULL,
	@expense_amount money = NULL,
	@Dates nvarchar(30) = NULL,
	@Create_Date nvarchar(25) = NULL,
	@Create_Date_UserName nvarchar(35) = NULL,
	@Update_Date nvarchar(25) = NULL,
	@Update_Date_UserName nvarchar(35) = NULL,
	@Delete_Date nvarchar(25) = NULL,
	@Delete_Date_UserName nvarchar(35) = NULL,
	@Delete_Why nvarchar(100) = NULL,
	@Type nvarchar(20) = null,
	@Date nvarchar(20) = null
) 
as
begin
  select @Date = CONVERT(varchar(10),getdate(),110)
  SELECT @AGE = CONVERT(int,ROUND(DATEDIFF(hour,@DOB,GETDATE())/8766.0,0)) -- AS AgeYearsIntRound
  if(@Type = 'Insert')
    begin
	  select @Sr_Number = ISNULL(max(Sr_Number),0)+1 from Admin_Register
	  select @Admin_id = ISNULL(max(Admin_id),100)+1 from Admin_Register
	  select @Admin_id = 'ADMIN_' + CAST(@Sr_Number as varchar(10))
	  select @Password_New = CAST(@Password as varbinary(max))
	  
	  insert into Admin_Register
						(Sr_Number,Office_ID,Admin_id,Admin_Name,Mobile,Email,UserName,Password,DOB,AGE,Other,Create_Date,Create_Date_UserName)
						Values
						(@Sr_Number,@Office_ID,@Admin_id,@Admin_Name,@Mobile,@Email,@UserName,@Password_New,@DOB,@AGE,@Other,@Date,@Create_Date_UserName)
	end

	if(@Type = 'Update')
    begin
	  select @Password_New = CAST(@Password as varbinary(max))
	  update Admin_Register
					Set Admin_Name = @Admin_Name , Mobile = @Mobile , Email = @Email , DOB = @DOB , AGE = @AGE ,
						UserName = @UserName , Password = @Password_New , Other = @Other ,
						Update_Date = @Date , Update_Date_UserName = @Update_Date_UserName
						where Admin_id = @Admin_id and Office_ID = @Office_ID
	end

	if(@Type = 'Delete')
    begin
	  update Admin_Register
					Set status = 0  , Delete_Why = @Delete_Why ,
						 Delete_Date = @Date , Delete_Date_UserName = @Delete_Date_UserName
						where Admin_id = @Admin_id  and Office_ID = @Office_ID
	end

	if(@Type = 'List_Active')
    begin
	  select * from Admin_Register where status = 1 and Office_ID = @Office_ID
	end

	if(@Type = 'List_InActive')
    begin
	  select * from Admin_Register where status = 0 and Office_ID = @Office_ID
	end

	if(@Type = 'Login')
    begin
      select  @Password_New = CAST(@Password as varbinary(max))
      -- select CONVERT(nvarchar(max), 0x4200680075007000650073006800)
	  select * from Admin_Register where UserName = @UserName and Password  = @Password and Office_ID = @Office_ID and status = 1
	end
end



GO
/****** Object:  StoredProcedure [dbo].[CRUD_Alteration]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[CRUD_Alteration]
(
	@Sr_Number int = NULL,
	@Office_ID nvarchar(20) = NULL,
	@Alteration_id int = NULL,
	@Deli_Date nvarchar(30) = NULL,
	@Name nvarchar(60) = NULL,
	@Mobile nvarchar(12) = NULL,
	@Perticular nvarchar(30) = NULL,
	@Alteration nvarchar(100) = NULL,
	@Rate money = Null,
	@Quantity int = null,
	@MRP_Quantity float  = Null,
	@Total_Amount money = Null,
	@Balance money = Null,
	@Advance Money = null,
	@Other nvarchar(100) = NULL,
	@Create_Date nvarchar(25) = NULL,
	@Create_Date_UserName nvarchar(35) = NULL,
	@Update_Date nvarchar(25) = NULL,
	@Update_Date_UserName nvarchar(35) = NULL,
	@Delete_Date nvarchar(25) = NULL,
	@Delete_Date_UserName nvarchar(35) = NULL,
	@Delete_Why nvarchar(100) = NULL,
	@Type nvarchar(20) = Null,
	@Date nvarchar(20) = Null
) 
as
begin
  select @Date = CONVERT(varchar(10),getdate(),110)
  if(@Type = 'Insert')
    begin
	  select @Sr_Number = ISNULL(max(Sr_Number),0)+1 from Alteration
	  select @Alteration_id = ISNULL(max(Alteration_id),100)+1 from Alteration
	  select @MRP_Quantity = @Quantity * @Rate
	  Select @Total_Amount = Sum(@MRP_Quantity) from Alteration where Name = @Name and Mobile = @Mobile
	  Select @Balance = @Total_Amount - @Advance

	  insert into Alteration
						(Sr_Number,Office_ID,Alteration_id,Deli_Date,Name,Mobile,Perticular,Alteration,Rate,Quantity,
						MRP_Quantity,Total_Amount,Balance,Advance,Other,Create_Date,Create_Date_UserName)
						Values
						(@Sr_Number,@Office_ID,@Alteration_id,@Deli_Date,@Name,@Mobile,@Perticular,@Alteration,@Rate,@Quantity,
						@MRP_Quantity,@Total_Amount,@Balance,@Advance,@Other,@Date,@Create_Date_UserName)
	end

	--if(@Type = 'Update')
 --   begin
	--  update Alteration
	--				Set Supplier_Name = @Supplier_Name , Mobile = @Mobile , Email = @Email , Other = @Other ,
	--					Update_Date = @Date , Update_Date_UserName = @Update_Date_UserName
	--					where Supplier_id = @Supplier_id and Office_ID = @Office_ID
	--end

	if(@Type = 'Delete')
    begin
	  update Alteration
					Set status = 0  , Delete_Why = @Delete_Why ,
						 Delete_Date = @Date , Delete_Date_UserName = @Delete_Date_UserName
						where Alteration_id = @Alteration_id  and Office_ID = @Office_ID
	end

	if(@Type = 'List_Active')
    begin
	  select * from Alteration where status = 1 and Office_ID = @Office_ID
	end

	if(@Type = 'List_InActive')
    begin
	  select * from Alteration where status = 0 and Office_ID = @Office_ID
	end
end
GO
/****** Object:  StoredProcedure [dbo].[CRUD_Customer]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[CRUD_Customer]
(
@Office_ID nvarchar(20) = null,
@sr_number int = null,
@Customer_ID int = null,
@Full_Name nvarchar(100) = null,
@Address nvarchar(100) = null,
@Mobile nvarchar(10) = null,
@Email nvarchar(50) = null,
@DOB nvarchar(25) = null,
@AGE int = Null,

@Create_Date nvarchar(25) = null,
@Create_Date_UserName nvarchar(35) = null,
@Update_Date nvarchar(25) = null,
@Update_Date_UserName nvarchar(35) = null,
@Delete_Date nvarchar(25) = null,
@Delete_Date_UserName nvarchar(35) = null,
@Delete_Why nvarchar(100) = null,
@Type nvarchar(20) = null,
@Date nvarchar(10) = null
)
as
begin
  select @Date = CONVERT(varchar(10),getdate(),110)
  SELECT @AGE = CONVERT(int,ROUND(DATEDIFF(hour,@DOB,GETDATE())/8766.0,0)) -- AS AgeYearsIntRound
  if(@Type = 'Insert')
  begin
    select @sr_number = ISNULL(max(sr_number),0)+1 from Customer
	select @Customer_ID = ISNULL(max(Customer_ID),100)+1 from Customer
	insert into Customer 
						(sr_number,Office_ID,Customer_ID,Full_Name,Address,Mobile,Email,DOB,AGE,Create_Date,Create_Date_UserName)
						values
						(@sr_number,@Office_ID,@Customer_ID,@Full_Name,@Address,@Mobile,@Email,@DOB,@AGE,@Date,@Create_Date_UserName)

  end

  if(@Type = 'Update')
  begin
	Update Customer
				  Set Full_Name = @Full_Name , Address = @Address , Mobile = @Mobile , Email = @Email ,
				  Update_Date = @Update_Date , Update_Date_UserName = @Update_Date_UserName , DOB = @DOB , AGE = @AGE 
				  where 
				  Customer_ID = @Customer_ID and Office_ID = @Office_ID

  end

  if(@Type = 'Delete')
  begin
	Update Customer
				  Set Status = 0 , Delete_Date = @Date , @Delete_Date_UserName = @Delete_Date_UserName
				  , Delete_Why = @Delete_Why
				  where 
				  Customer_ID = @Customer_ID and Office_ID = @Office_ID

  end

  if(@Type = 'List_Active')
    begin
	  select * from Customer where status = 1 and Office_ID = @Office_ID
	end

  if(@Type = 'List_InActive')
	begin
		select * from Customer where status = 0 and Office_ID = @Office_ID
	end
end
GO
/****** Object:  StoredProcedure [dbo].[CRUD_Expense]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[CRUD_Expense]
(
    @Sr_Number int = NULL,
	@Office_ID nvarchar(20) = null,
	@Expense_id int = NULL,
	@Category nvarchar(30) = NULL,
	@Other nvarchar(100) = NULL,
	@income_amount money = NULL,
	@expense_amount money = NULL,
	@Dates nvarchar(30) = NULL,
	@Create_Date nvarchar(25) = NULL,
	@Create_Date_UserName nvarchar(35) = NULL,
	@Update_Date nvarchar(25) = NULL,
	@Update_Date_UserName nvarchar(35) = NULL,
	@Delete_Date nvarchar(25) = NULL,
	@Delete_Date_UserName nvarchar(35) = NULL,
	@Delete_Why nvarchar(100) = NULL,
	@Type nvarchar(20) = null,
	@Date nvarchar(20) = null
) 
as
begin
  select @Date = CONVERT(varchar(10),getdate(),110)
  if(@Type = 'Insert')
    begin
	  select @Sr_Number = ISNULL(max(Sr_Number),0)+1 from Expense
	  select @Expense_id = ISNULL(max(Expense_id),100)+1 from Expense
	  insert into Expense
						(Sr_Number,Office_ID,Expense_id,Category,Other,income_amount,expense_amount,Dates,Create_Date,Create_Date_UserName)
						Values
						(@Sr_Number,@Office_ID,@Expense_id,@Category,@Other,@income_amount,@expense_amount,@Dates,@Date,@Create_Date_UserName)
	end

	if(@Type = 'Update')
    begin
	  update Expense
					Set Category = @Category , Other = @Other , income_amount = @income_amount , expense_amount = @expense_amount ,
						Dates = @Dates , Update_Date = @Date , Update_Date_UserName = @Update_Date_UserName
						where Expense_id = @Expense_id and Office_ID = @Office_ID
	end

	if(@Type = 'Delete')
    begin
	  update Expense
					Set status = 0  , Delete_Why = @Delete_Why ,
						 Delete_Date = @Date , Delete_Date_UserName = @Delete_Date_UserName
						where Expense_id = @Expense_id  and Office_ID = @Office_ID
	end

	if(@Type = 'List_Active')
    begin
	  select * from Expense where status = 1 and Office_ID = @Office_ID
	end

	if(@Type = 'List_InActive')
    begin
	  select * from Expense where status = 0 and Office_ID = @Office_ID
	end
end


GO
/****** Object:  StoredProcedure [dbo].[CRUD_Perticular]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[CRUD_Perticular]
(
    @Sr_Number int = NULL,
	@Office_ID nvarchar(20) = null,
	
	@Perticular_id int = NULL,
	@Perticular nvarchar(30) = NULL,
	@GroupName nvarchar(30) = NULL,

	@Other nvarchar(100) = NULL,
	@Create_Date nvarchar(25) = NULL,
	@Create_Date_UserName nvarchar(35) = NULL,
	@Update_Date nvarchar(25) = NULL,
	@Update_Date_UserName nvarchar(35) = NULL,
	@Delete_Date nvarchar(25) = NULL,
	@Delete_Date_UserName nvarchar(35) = NULL,
	@Delete_Why nvarchar(100) = NULL,
	@Type nvarchar(20) = null,
	@Date nvarchar(20) = null
) 
as
begin
  select @Date = CONVERT(varchar(10),getdate(),110)
  if(@Type = 'Insert')
    begin
	  select @Sr_Number = ISNULL(max(Sr_Number),0)+1 from Perticular
	  select @Perticular_id = ISNULL(max(Perticular_id),100)+1 from Perticular
	  insert into Perticular
						(Sr_Number,Office_ID,Perticular_id,Perticular,GroupName,Other,Create_Date,Create_Date_UserName)
						Values
						(@Sr_Number,@Office_ID,@Perticular_id,@Perticular,@GroupName,@Other,@Date,@Create_Date_UserName)
	end

	if(@Type = 'Update')
    begin
	  update Perticular
					Set Perticular = @Perticular , GroupName = @GroupName , Other = @Other ,
						Update_Date = @Date , Update_Date_UserName = @Update_Date_UserName
						where Perticular_id = @Perticular_id and Office_ID = @Office_ID
	end

	if(@Type = 'Delete')
    begin
	  update Perticular
					Set status = 0  , Delete_Why = @Delete_Why ,
						 Delete_Date = @Date , Delete_Date_UserName = @Delete_Date_UserName
						where Perticular_id = @Perticular_id  and Office_ID = @Office_ID
	end

	if(@Type = 'List_Active')
    begin
	  select * from Perticular where status = 1 and Office_ID = @Office_ID
	end

	if(@Type = 'List_InActive')
    begin
	  select * from Perticular where status = 0 and Office_ID = @Office_ID
	end
end



GO
/****** Object:  StoredProcedure [dbo].[CRUD_SendMessages]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[CRUD_SendMessages]
(
    @Sr_Number int = NULL,
	@Office_ID nvarchar(20) = null,
	@Messages_id int = NULL,
	@Messages nvarchar(200) = NULL,
	@Other nvarchar(100) = NULL,
	@income_amount money = NULL,
	@expense_amount money = NULL,
	@Dates nvarchar(30) = NULL,
	@Create_Date nvarchar(25) = NULL,
	@Create_Date_UserName nvarchar(35) = NULL,
	@Update_Date nvarchar(25) = NULL,
	@Update_Date_UserName nvarchar(35) = NULL,
	@Delete_Date nvarchar(25) = NULL,
	@Delete_Date_UserName nvarchar(35) = NULL,
	@Delete_Why nvarchar(100) = NULL,
	@Type nvarchar(20) = null,
	@Date nvarchar(20) = null
) 
as
begin
  select @Date = CONVERT(varchar(10),getdate(),110)
  if(@Type = 'Insert')
    begin
	  select @Sr_Number = ISNULL(max(Sr_Number),0)+1 from SendMessages
	  select @Messages_id = ISNULL(max(Messages_id),100)+1 from SendMessages
	  insert into SendMessages
						(Sr_Number,Office_ID,Messages_id,Messages,Other,Create_Date,Create_Date_UserName)
						Values
						(@Sr_Number,@Office_ID,@Messages_id,@Messages,@Other,@Date,@Create_Date_UserName)
	end

	if(@Type = 'Update')
    begin
	  update SendMessages
					Set Messages = @Messages , Other = @Other ,
						Update_Date = @Date , Update_Date_UserName = @Update_Date_UserName
						where Messages_id = @Messages_id and Office_ID = @Office_ID
	end

	if(@Type = 'Delete')
    begin
	  update SendMessages
					Set status = 0  , Delete_Why = @Delete_Why ,
						 Delete_Date = @Date , Delete_Date_UserName = @Delete_Date_UserName
						where Messages_id = @Messages_id  and Office_ID = @Office_ID
	end

	if(@Type = 'List_Active')
    begin
	  select * from SendMessages where status = 1 and Office_ID = @Office_ID
	end

	if(@Type = 'List_InActive')
    begin
	  select * from SendMessages where status = 0 and Office_ID = @Office_ID
	end
end



GO
/****** Object:  StoredProcedure [dbo].[CRUD_Supplier]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[CRUD_Supplier]
(
    @Sr_Number int = NULL,
	@Office_ID nvarchar(20) = null,
	@Supplier_id int = NULL,

	@Supplier_Name nvarchar(100) = null,
	@Company_Name nvarchar(50) = null,
	@Mobile nvarchar(12) = null,
	@Email nvarchar(40) = null,

	@Other nvarchar(100) = NULL,
	@income_amount money = NULL,
	@expense_amount money = NULL,
	@Dates nvarchar(30) = NULL,
	@Create_Date nvarchar(25) = NULL,
	@Create_Date_UserName nvarchar(35) = NULL,
	@Update_Date nvarchar(25) = NULL,
	@Update_Date_UserName nvarchar(35) = NULL,
	@Delete_Date nvarchar(25) = NULL,
	@Delete_Date_UserName nvarchar(35) = NULL,
	@Delete_Why nvarchar(100) = NULL,
	@Type nvarchar(20) = null,
	@Date nvarchar(20) = null
) 
as
begin
  select @Date = CONVERT(varchar(10),getdate(),110)
  if(@Type = 'Insert')
    begin
	  select @Sr_Number = ISNULL(max(Sr_Number),0)+1 from Supplier
	  select @Supplier_id = ISNULL(max(Supplier_id),100)+1 from Supplier
	  insert into Supplier
						(Sr_Number,Office_ID,Supplier_id,Supplier_Name,Company_Name,Mobile,Email,Other,Create_Date,Create_Date_UserName)
						Values
						(@Sr_Number,@Office_ID,@Supplier_id,@Supplier_Name,@Company_Name,@Mobile,@Email,@Other,@Date,@Create_Date_UserName)
	end

	if(@Type = 'Update')
    begin
	  update Supplier
					Set Supplier_Name = @Supplier_Name , Mobile = @Mobile , Email = @Email , Other = @Other ,
						Update_Date = @Date , Update_Date_UserName = @Update_Date_UserName
						where Supplier_id = @Supplier_id and Office_ID = @Office_ID
	end

	if(@Type = 'Delete')
    begin
	  update Supplier
					Set status = 0  , Delete_Why = @Delete_Why ,
						 Delete_Date = @Date , Delete_Date_UserName = @Delete_Date_UserName
						where Supplier_id = @Supplier_id  and Office_ID = @Office_ID
	end

	if(@Type = 'List_Active')
    begin
	  select * from Supplier where status = 1 and Office_ID = @Office_ID
	end

	if(@Type = 'List_InActive')
    begin
	  select * from Supplier where status = 0 and Office_ID = @Office_ID
	end
end



GO
/****** Object:  StoredProcedure [dbo].[insert_ledger_All]    Script Date: 21-06-2019 13:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER procedure [dbo].[insert_ledger_All]
(
 @sr_number int = null,
 @Office_ID nvarchar(20) = NULL,
 @ledger_id nvarchar(10) = null,
 @voucher_id nvarchar(30) = null,
 @save_date nvarchar(30) = null,
 @cust_supp_id nvarchar(14) = null,
 @party_name nvarchar(70) = null,
 @bill_amount nvarchar(14) = null,
 @paid_amount nvarchar(14) = null,
 @balance_amount nvarchar(15) = null,
 @remark nvarchar(100) = null,
 @save_time nvarchar(14) = null,
 @date nvarchar(30) = null,
 @type nvarchar(30) = null
)
as
select  @date = CONVERT(varchar(30),getdate(),103)
 select @sr_number = ISNULL(max(sr_number),0)+1 from ledger 
 select @ledger_id = ISNULL(max(ledger_id),100)+1 from ledger 
if(@type = 'insert_sale')
begin
 insert into ledger 
				 (sr_number,Office_ID,ledger_id,voucher_id,save_date,cust_supp_id,party_name,bill_amount,paid_amount,balance_amount) values
				 (@sr_number,@Office_ID,@ledger_id,'SALE-'+@ledger_id,@date,@cust_supp_id,@party_name,@bill_amount,@paid_amount,@balance_amount)  
end

else if(@type = 'insert_ledger')
begin
 SELECT @balance_amount = 0 - cast(@paid_amount as float)
 insert into ledger 
				 (sr_number,Office_ID,ledger_id,voucher_id,save_date,cust_supp_id,party_name,bill_amount,paid_amount,balance_amount,remark,save_time) values
				 (@sr_number,@Office_ID,@ledger_id,'CASH-'+@ledger_id,@date,@cust_supp_id,@party_name,0,@paid_amount,@balance_amount,@remark,@save_time)  
end
else if(@type = 'showGrid')
begin
 select ledger_id as ID,voucher_id as Vou_ID,save_date as Date,party_name as Name,bill_amount as Bill_Amt,paid_amount as Paid_Amt,balance_amount as Bal_Amt from  ledger where party_name= @party_name and Office_ID =@Office_ID order by sr_number asc
end
GO
