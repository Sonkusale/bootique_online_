﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Logic_layer
{
    public class Error_BLL
    {
        public int ErrorLogId { get; set; }
        public string Url { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string ErrorDateTime { get; set; }
    }
}
