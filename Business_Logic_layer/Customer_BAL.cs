﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Logic_layer
{
    public class Customer_BAL
    {
        public int? sr_number { get; set; }
        public int? Customer_ID { get; set; }
        public string Full_Name { get; set; }
        public string Address { get; set; }
        public string Mobile { get; set; }
        public string DOB { get; set; }
        public string Email { get; set; }
        public string Delete_Why { get; set; }
    }
}
