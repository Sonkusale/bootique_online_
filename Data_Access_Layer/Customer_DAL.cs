﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Business_Logic_layer;

namespace Data_Access_Layer
{
    public class Customer_DAL
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
        string User_Name = System.Configuration.ConfigurationManager.AppSettings.Get("UserName");        string Office_ID = System.Configuration.ConfigurationManager.AppSettings.Get("OfficeID");
        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        string UserName = string.Empty;
        ErrorLog_DAL errors = new ErrorLog_DAL();

        public string Add_Customer(Customer_BAL cust)
        {
            try
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
                con.Open();
                cmd = new SqlCommand();
                cmd.CommandText = "CRUD_Customer";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;

                cmd.Parameters.Add("@Type", SqlDbType.NVarChar).Value = "Insert";
                cmd.Parameters.Add("@Full_Name", SqlDbType.NVarChar).Value = cust.Full_Name;
                cmd.Parameters.Add("@Address", SqlDbType.NVarChar).Value = cust.Address;
                cmd.Parameters.Add("@Mobile", SqlDbType.NVarChar).Value = cust.Mobile;
                cmd.Parameters.Add("@DOB", SqlDbType.NVarChar).Value = cust.DOB;
                cmd.Parameters.Add("@Email", SqlDbType.NVarChar).Value = cust.Email;
                cmd.Parameters.Add("@Create_Date_UserName", SqlDbType.NVarChar).Value = User_Name;
                cmd.Parameters.Add("@Office_ID", SqlDbType.NVarChar).Value = Office_ID;
                
                cmd.ExecuteNonQuery();
                con.Close();
                return "Success";
            }
            catch(Exception ex)
            {
                Error_BLL ers = new Error_BLL();
                ers.Url = "MasterController --> AddCustomer";
                ers.Message = ex.Message;
                ers.StackTrace = ex.StackTrace;
                var result = errors.Insert_Error(ers);
                return "Fail";
            }
            
        }
        public DataTable GetAll_Customer()
        {
            dt = new DataTable();
            try
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
                con.Open();
                cmd = new SqlCommand();
                cmd.CommandText = "CRUD_Customer";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;

                cmd.Parameters.Add("@Type", SqlDbType.NVarChar).Value = "List_Active";
                cmd.Parameters.Add("@Office_ID", SqlDbType.NVarChar).Value = Office_ID;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                Error_BLL ers = new Error_BLL();
                ers.Url = "MasterController --> GetAll_Customer";
                ers.Message = ex.Message;
                ers.StackTrace = ex.StackTrace;
                var result = errors.Insert_Error(ers);
                return dt;
            }
        }
    }
}
