﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Business_Logic_layer;

namespace Data_Access_Layer
{
    public class ErrorLog_DAL
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Connection"].ConnectionString);
        
        SqlCommand cmd = new SqlCommand();
        DataTable dt;
        string UserName = string.Empty;
        Error_BLL errors = new Error_BLL();

        public bool Insert_Error(Error_BLL errors)
        {
            try
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
                con.Open();
                cmd = new SqlCommand();
                cmd.CommandText = "InsertErrorLog";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;

                cmd.Parameters.Add("@Url", SqlDbType.NVarChar).Value = errors.Url;
                cmd.Parameters.Add("@ErrorMessage", SqlDbType.NVarChar).Value = errors.Message;
                cmd.Parameters.Add("@StackTrace", SqlDbType.NVarChar).Value = errors.StackTrace;
                cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
